\section{Approach}
The goal in this project was to predict sale prices of some items based on past purchases and the features and specification of those items. Given an input data our task was to predict the sale price.\\
The task was a typical data analysis problem. The training dataset contained 401125 items with 53 features among them Sale price which we were trying to make predictions about.\\
About 12 features were completely specified in the training dataset. Among these features were the date in which the sale was made, the state in which the item was sold and description of the models. The remaining features were either missing or not applicable. This can be seen in figure \ref{fig:missing} which demonstrates the percentage of missing values. \\
\begin{figure}\centering
\includegraphics[scale=0.5]{missing.png}
\caption{Percentage of filled values in each feature in complete dataset.}
\label{fig:missing}
\end{figure}

Working directly on this dataset was difficult and time consuming. Moreover, having too many missing values for a particular feature would reduce the quality of predictions for almost any learning model, therefore we either had to drop those features that had very high missing rate or limit ourselves to only learning algorithms that are capable of working with inputs that have missing values.\\
At this stage we had two major obstacles. First, how to reduce the size of datasets so that we could rapidly perform different learning algorithms on ordinary PCs and second how to reduce the amount of missing values on data that was fed into those learning method. One of the features that was specified for all input data was Model ID. Each item had a model ID and that was shared among all items of the same model. We split the original dataset into smaller datasets each containing only items of same model.\\
We can observe from figures \ref{fig:modelsize} and \ref{fig:modelmissing} that we got datasets that are smaller and denser than the original dataset, from \%62 to \%14. This was also conceptually meaningful because we would expect different predicting models to perform differently on different item type.\\
\begin{figure}
        \centering
        \subfigure[Log count of samples in each model.]{\label{fig:modelsize}\includegraphics[scale=0.4]{modelscount.png}}
        \subfigure[Mean number of filled values in each model.]{
\includegraphics[scale=0.4]{models.png}
\label{fig:modelmissing}
}

\caption{Number of samples in each machine model and average number of filled values.}
\end{figure}

Even though this approach is conceptually effective, as we will see later, there is also a caveat. For many models we have only a few samples. This will be troublesome when a predicting model uses very small number of training data.\\
\subsection{Feature Transformation}
Splitting the original dataset by Model ID produced 5218 datasets. We developed a pipeline infrastructure that would transform any input into a format that can be used for various learning algorithms, figure \ref{fig:pipeline}. This infrastructure made it easy to for us to parallelize training of many models on our multi-core systems.
\begin{figure}\centering
\includegraphics[scale=0.5]{pipeline.png}
\caption{Learning pipeline for each model.}
\label{fig:pipeline}
\end{figure}
These steps are described below:
\begin{itemize}
\item \textbf{Picking none-unique Features}: The first step consists of discarding those features that did not have any values or all the values are the same. Recall that when we split the dataset into smaller pieces for each model many features have similar values for the items of same type or do not have any values at all.\\
\item \textbf{Transforming dates to year and month}: Some columns contain dates that are represented as strings in the dataset. At this step we extract those dates and convert them into years and months.\\
\item \textbf{Converting categorical data to integers}: At this step we transform all the non-number features into integers. All the similar values in each column is mapped into a unique number in each feature.
\item \textbf{Normalization}: The final step is to convert features into values with mean zero and variance 1. This is a requirement for many learning algorithms such as SVM and Linear Regression. This step avoids dominance of a feature only because of its range domain.
\item \textbf{Learning Algorithms}: The final step is a learning algorithm that will make predictions.
\end{itemize}
\subsection{Learning Algorithms}
The final stage of the pipeline described before is the learning model. We tried various methods of regression on the dataset. The results of these methods are presented in subsequent sections. In this part we breifly describe these algorithms.
\begin{itemize}
\item \textbf{Ridge Regression}: Ridge Regression is an extension of ordinary linear regression with a regularization term that tries to avoid overfitting. This model is effective if the function we are trying to model is linear in terms of its inputs. Optimization function for this model can be described as:
\[
min_w ||Y-WX||_2 +\lambda||W||_2^2 
\]
The $\lambda$ term in thid model indicates degree of regularization. if $\lambda=0$ we will recover the original linear regression formulation.
\item \textbf{Lasso\cite{tibshirani1996regression}}: Lasso is another way of regularizing linear regression. While Ridge uses L2 norm for regularization, Lasso uses L1 norm which is more robust compared to Ridge. We should note that both of these optimizations problems are convex and any gradient based method will find $W$s that are globally optimum. The optimization problem for Lasso can be formulated as:
\[
min_w ||Y-WX||_2 +\lambda||W||_1 
\]
\item \textbf{SVM for Regression\cite{basak2007support}}: This algorithm is a generalization of Support Vector Machines for regression problems. The formulation is similar to SVM for non-linearly separable data. SVM for regression has the same drawbacks of original SVM. Particularly they do not scale with size of inputs and has the complexity of $O(n^3)$ where $n$ is number of training examples.
\item \textbf{Decision Tree}: Decision Tree is a method that is used generally used	 for classification. There is, however, an adaptation of this algorithm that can be used for regression problems. This algorithm is very similar to conventional decision trees and has tunning parameter for maximum depth of the tree.

\item \textbf{Random Forest Decision Trees}: Random Forests are a collection of algorithms that construct several decision trees and take an average on the result of all the decision trees. Decision Trees are not pruned and at each node a random set of features are used to find the best feature for splitting.

\end{itemize}
These algorithms are depicted in figure \ref{fig:all}.
\begin{figure}
        \centering
        \subfigure[Random Forests and Decision Trees.]{
                \includegraphics[scale=0.3]{RF-DT}
                \label{fig:rfdt}
                }
        \subfigure[Lasso, Ridge and RSVM.]{
                        \centering
                        \includegraphics[scale=0.3]{Lasso-Ridge-SVM}
                        \label{fig:rfdt}
}
\caption{Performance of different methods of learning used in this project on synthetic data, the actual function is $sin(.)$.}\label{fig:all}
\end{figure}
