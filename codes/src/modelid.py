# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

#%reset
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import transformer
from sklearn.cross_validation import KFold
from sklearn.cross_validation import LeaveOneOut
from sklearn.ensemble import RandomForestRegressor
from sklearn import preprocessing
from sklearn.pipeline import Pipeline
from sklearn import linear_model
from sklearn import gaussian_process
from sklearn import svm
from sklearn import tree
from sklearn.naive_bayes import MultinomialNB
from sklearn.externals import joblib
import glob

# <codecell>

class NBmodel:
    def __init__(self):
        pass
    def fit(self,X,Y):
        labels=Y.value_counts()
        #print labels
        features=dict([])
        sudo=1
        for col in X.columns:
            vals=X[col].value_counts()
            if (vals.shape[0]>10):
                print 'discarding', col, vals.shape[0] 
                continue
            col_lab=pd.DataFrame(np.zeros((labels.shape[0],vals.shape[0])),index=labels.index,columns=vals.index).T
            #print col_lab
            #print 'columns value',vals
            #print 'class label',labels
            for j in vals.index:
                for lab in labels.index:
                    #print j,lab
                    index= Y==lab
                    #print index
                    X_t=X[index]
                    col_lab[lab][j]=sudo+X_t[col][X_t[col]==j].shape[0]
            print 'feature, ',col,' completed!'
            features[col]=col_lab
        self.features=features
        self.labels=labels
        #print self.labels
        #print self.features
        return self
    def predict(self,X):
        p_z_given_x=pd.DataFrame(np.ones((X.shape[0],self.labels.shape[0])),index=X.index,columns=self.labels.index)
        for z in self.labels.index:
            for col in self.features:
                matrix= self.features[col]
                #print matrix
                p= np.log(matrix[z][X[col]]/matrix[z].sum())
                p.index=X.index
                #print p
                p_z_given_x[z]=p+p_z_given_x[z]
                #print p_z_given_x[z]
            p_z_given_x[z]=np.log(self.labels[z]*1.0/self.labels.sum())#+p_z_given_x[z]
        return p_z_given_x.idxmax(axis=1)
            
                
    
X = pd.DataFrame(np.random.randint(5, size=(6, 100)))
Y = pd.Series(np.array([1, 2, 3, 4, 3, 2]))
model=NBmodel()
model.fit(X,Y)
print model.predict(X)
print 'truth'
print Y

# <codecell>

models=[]
lst=np.zeros((0,2))
count=3000
for i in glob.glob("../dataset/Train.csv"):
    if count>0:
        count-=1
    else:
        break
    print i
    s=0
    k=10
    df=pd.read_csv(i,na_values=["None or Unspecified"])
    #print df
    features=df.columns-['ModelID','MachineID']
    #features=features[]
    if not np.any(df.columns=='SalePrice'):
        df=df.T
    kf=KFold(df.shape[0],k=k,shuffle=True,indices=False)
    for train,test in kf:
        X=df[features][train]
        miss=transformer.Missingtransform()
        trans=transformer.Categoricaltransform()
        mult=transformer.MultiTrans()
        norm=preprocessing.Scaler()
        reg=tree.DecisionTreeClassifier(max_depth=40)#NBmodel()
        data = Pipeline([('mult',mult),('date',transformer.DateTrans()),('cat', trans),('miss',miss),('reg',reg)])#
        print 'fitting'
        data.fit(X,df['ModelID'][train])
        print 'predicting'
        
        joblib.dump(data, 'filename.pkl') 
        data2=joblib.load('filename.pkl')
        print np.sum(data2.predict(df[features][test])==df['ModelID'][test])
        #
        #predict=anova_svm.predict(df[features][test]).reshape((df[features][test].shape[0],1))
        #res=np.hstack((np.asarray(df['SalePrice'][test]).reshape((df[features][test].shape[0],1)),predict))
            
        #lst=np.vstack((lst,res))
        #df=None
        print 'end of fold'
        
#print score_cus(lst[:,0],lst[:,1])

