# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

#reset
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import transformer
from sklearn.cross_validation import KFold
from sklearn.cross_validation import LeaveOneOut
from sklearn.ensemble import RandomForestRegressor
from sklearn import preprocessing
from sklearn.pipeline import Pipeline
from sklearn import linear_model
from sklearn import gaussian_process
from sklearn import svm
from sklearn import tree
from joblib import Parallel, delayed,Memory
from dateutil.parser import parse
import glob

# <codecell>

class meanPred:
    def __init__(self):
        pass
    def fit(self,X,Y):
        self.mean=np.mean(Y,axis=1)
        return self
    def predict(self,X):
        return np.ones(X.shape[0])*self.mean
    def transform(self,X):
        return X

# <codecell>

def score_cus(Y_true,Y_pred):
    return np.sqrt((1.0/Y_true.shape[0])*np.dot((np.log(Y_pred+1)-np.log(Y_true+1)).T, \
    np.log(Y_pred+1)-np.log(Y_true+1)))

# <codecell>

import mlpy
class kernelReg:
    def __init__(self):
        self.SIGMA=1
    def fit(self,X,Y):
        #print X.shape
        K = mlpy.kernel_gaussian(X, X, sigma=self.SIGMA)
        
        self.X=X
        self.krr = mlpy.KernelRidge(lmb=10)
        self.krr.learn(K, Y)
        return self
    def predict(self,X):
        Kt = mlpy.kernel_gaussian(X, self.X, sigma=self.SIGMA)
        
        yt = self.krr.pred(Kt)
        
        return yt

# <codecell>

def model():
    df=pd.read_csv("../dataset/Train.csv",na_values=["None or Unspecified"])
    #print df
    features=df.columns-['ModelID']
    #features=features[]
    X=df[features]
    miss=transformer.Missingtransform()
    trans=transformer.Categoricaltransform()
    mult=transformer.MultiTrans()
    norm=preprocessing.Scaler()
    reg=tree.DecisionTreeClassifier(max_depth=40)
    data = Pipeline([('mult',mult),('date',transformer.DateTrans()),('cat', trans),('miss',miss),('reg',reg)])#
    data.fit(X,df['ModelID'])
    return [-1,data]

# <codecell>

def compute_model(fi):
    s=0
    k=10
    df=pd.read_csv(fi,na_values=['None or Unspecified'])
    if not np.any(df.columns=='SalePrice'):
        df.set_index(df.columns[0],inplace=True)
        new_df=df.T
        new_df.reset_index(inplace=True)
        df=new_df
    df.YearMade=df.YearMade.astype('float')
    df.YearMade[df.YearMade==1000]=np.NaN
    features=df.columns-['SalePrice']
    
    X=df[features]
    miss=transformer.Missingtransform()
    trans=transformer.Categoricaltransform()
    mult=transformer.MultiTrans()
    norm=preprocessing.Scaler()
    #
    me=meanPred()
    if X.shape[0]<10:
        anova_svm = Pipeline([('mult',mult),('date',transformer.DateTrans()),('cat', trans),('miss', miss),('norm',norm),('reg',me)])#
    elif X.shape[0]<400:
        reg=RandomForestRegressor(n_estimators=25)
        anova_svm = Pipeline([('mult',mult),('date',transformer.DateTrans()),('cat', trans),('miss', miss),('norm',norm),('reg',reg)])#
    else:
        reg=RandomForestRegressor(n_estimators=50)
        anova_svm = Pipeline([('mult',mult),('date',transformer.DateTrans()),('cat', trans),('miss', miss),('norm',norm),('reg',reg)])#
        
        
    anova_svm.fit(X,df['SalePrice'])
    return [int(fi.split('\\')[-1].replace('.csv','')),(anova_svm,df['SalePrice'].mean())]

# <codecell>

if __name__=='__main__':
    files=glob.glob("../dataset/model/*.csv")
    cachedir = '../cache/'
    memory = Memory(cachedir=cachedir, verbose=0)
    tasks=[]
    tasks=[delayed(compute_model)(i) for i in files[0:]]
    tasks.insert(100,delayed(memory.cache(model))())
    r=Parallel(n_jobs=8,verbose=40)(tasks)
    diction=dict(r)
    Valid=pd.read_csv('../dataset/Valid.csv',na_values=['None or Unspecified'])
    lst=np.zeros((0,2))
    Valid["predicted_model"]=-1
    for i in Valid.ModelID.unique():
        if diction.has_key(i):
            try:
                predict=diction[i][0].predict(Valid[Valid.ModelID==i]).reshape(Valid[Valid.ModelID==i].shape[0],1)
                predict[predict<1000]=diction[i][1]
                pred=np.hstack((predict,Valid[Valid.ModelID==i].SalePrice.reshape(predict.shape[0],1)))
                lst=np.vstack((pred,lst))
            except:
                print 'error!'
                #pred=np.hstack((np.ones((predict.shape[0],1))*31099,Valid[Valid.ModelID==i].SalePrice.reshape(predict.shape[0],1)))
                #lst=np.vstack((pred,lst))
                break
        else:
            continue
            pred=diction[-1].predict(Valid[Valid.ModelID==i]).astype('int')
            Valid[Valid.ModelID==i].predicted_model=pred
            for k in pred:
                predict=diction[k][0].predict(Valid[Valid.ModelID==i & Valid.predicted_model== k]).reshape(Valid[Valid.ModelID==i & Valid.predicted_model== k].shape[0],1)
                pred=np.hstack((predict,Valid[Valid.ModelID==i].SalePrice.reshape(predict.shape[0],1)))
                lst=np.vstack((pred,lst))
    print lst
    print score_cus(lst[:,1],lst[:,0])

