# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

#reset
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import transformer
from sklearn.cross_validation import KFold
from sklearn.cross_validation import LeaveOneOut
from sklearn.ensemble import RandomForestRegressor
from sklearn import preprocessing
from sklearn.pipeline import Pipeline
from sklearn import linear_model
from sklearn import gaussian_process
from sklearn import svm
from sklearn import tree
from joblib import Parallel, delayed,Memory
import glob

# <codecell>

def compute_model(fi):
    s=0
    k=10
    df=pd.read_csv(fi,na_values=['None or Unspecified'])
    if not np.any(df.columns=='SalePrice'):
        df.set_index(df.columns[0],inplace=True)
        new_df=df.T
        new_df.reset_index(inplace=True)
        df=new_df
    df.YearMade=df.YearMade.astype('float')
    df.YearMade[df.YearMade==1000]=np.NaN
    features=df.columns-['SalePrice']
    
    X=df[features]
    miss=transformer.Missingtransform()
    trans=transformer.Categoricaltransform()
    mult=transformer.MultiTrans()
    norm=preprocessing.Scaler()
    #
    reg=RandomForestRegressor(n_estimators=50)
    anova_svm = Pipeline([('mult',mult),('date',transformer.DateTrans()),('cat', trans),('miss', miss),('norm',norm),('reg',reg)])#
    anova_svm.fit(X,df['SalePrice'])
    return anova_svm
baseline=compute_model("../dataset/Train.csv")
Valid=pd.read_csv('../dataset/Valid.csv',na_values=['None or Unspecified'])
predict=baseline.predict(Valid).reshape(Valid.shape[0],1)
pred=np.hstack((predict,Valid[Valid.ModelID==i].SalePrice.reshape(predict.shape[0],1)))
print score_cus(pred[:,1],pred[:,0])

