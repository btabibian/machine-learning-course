# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

#%reset
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import transformer
from sklearn.cross_validation import KFold
from sklearn.cross_validation import LeaveOneOut
from sklearn.ensemble import RandomForestRegressor
from sklearn import preprocessing
from sklearn.pipeline import Pipeline
from sklearn import linear_model
from sklearn import gaussian_process
from sklearn import svm
from sklearn import tree
from sklearn.naive_bayes import GaussianNB
import glob

# <codecell>

class meanPred:
    def __init__(self):
        pass
    def fit(self,X,Y):
        self.mean=np.mean(Y,axis=1)
        return self
    def predict(self,X):
        return np.ones(X.shape[0])*self.mean
    def transform(self,X):
        return X

# <codecell>

def score_cus(Y_true,Y_pred):
    return np.sqrt((1.0/Y_true.shape[0])*np.dot((np.log(Y_pred+1)-np.log(Y_true+1)).T, \
    np.log(Y_pred+1)-np.log(Y_true+1)))

# <codecell>

import mlpy
class kernelReg:
    def __init__(self):
        self.SIGMA=1
    def fit(self,X,Y):
        #print X.shape
        K = mlpy.kernel_gaussian(X, X, sigma=self.SIGMA)
        
        self.X=X
        self.krr = mlpy.KernelRidge(lmb=10)
        self.krr.learn(K, Y)
        return self
    def predict(self,X):
        Kt = mlpy.kernel_gaussian(X, self.X, sigma=self.SIGMA)
        
        yt = self.krr.pred(Kt)
        
        return yt

# <codecell>

models=[]
lst=np.zeros((0,2))
count=3000
for i in glob.glob("../dataset/model/*.csv"):
    if count>0:
        count-=1
    else:
        break
    print i
    s=0
    k=10
    df=pd.read_csv(i)
    features=df.columns-['SalePrice','MachineID']
    features=features[0:10]
    if not np.any(df.columns=='SalePrice'):
        df=df.T
    if df.shape[0]<3:
        continue
    if df.shape[0]<15:
        k=2
    kf=KFold(df.shape[0],k=k,indices=False)
    for train,test in kf:
        X=df[features][train]
        miss=transformer.Missingtransform()
        trans=transformer.Categoricaltransform()
        mult=transformer.MultiTrans()
        norm=preprocessing.Scaler()
        reg=tree.DecisionTreeRegressor(min_samples_leaf=10)
        me=meanPred()
        if X.shape[0]<5:
            print 'mean activated'
            anova_svm = Pipeline([('mult',mult),('date',transformer.DateTrans()),('miss', miss),('cat', trans),('norm',norm),('reg',me)])#
        else:
            anova_svm = Pipeline([('mult',mult),('date',transformer.DateTrans()),('miss', miss),('cat', trans),('norm',norm),('reg',reg)])#
        
        anova_svm.fit(X,df['SalePrice'][train])
        predict=anova_svm.predict(df[features][test]).reshape((df[features][test].shape[0],1))
        res=np.hstack((np.asarray(df['SalePrice'][test]).reshape((df[features][test].shape[0],1)),predict))
        
        lst=np.vstack((lst,res))
    df=None
print score_cus(lst[:,0],lst[:,1])

