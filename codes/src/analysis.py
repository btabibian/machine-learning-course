# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

#%reset
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import transformer
from sklearn.cross_validation import KFold
from sklearn.cross_validation import LeaveOneOut
from sklearn.ensemble import RandomForestRegressor
from sklearn import preprocessing
from sklearn.pipeline import Pipeline
from sklearn import linear_model
from sklearn import gaussian_process
from sklearn import svm
from sklearn import tree
from sklearn.naive_bayes import GaussianNB
import glob

# <codecell>

class meanPred:
    def __init__(self):
        pass
    def fit(self,X,Y):
        self.mean=np.mean(Y,axis=1)
        return self
    def predict(self,X):
        return np.ones(X.shape[0])*self.mean
    def transform(self,X):
        return X
def score_cus(Y_true,Y_pred):
    return np.sqrt((1.0/Y_true.shape[0])*np.dot((np.log(Y_pred+1)-np.log(Y_true+1)).T, \
    np.log(Y_pred+1)-np.log(Y_true+1)))
import mlpy
class kernelReg:
    def __init__(self):
        self.SIGMA=1
    def fit(self,X,Y):
        #print X.shape
        K = mlpy.kernel_gaussian(X, X, sigma=self.SIGMA)
        
        self.X=X
        self.krr = mlpy.KernelRidge(lmb=10)
        self.krr.learn(K, Y)
        return self
    def predict(self,X):
        Kt = mlpy.kernel_gaussian(X, self.X, sigma=self.SIGMA)
        
        yt = self.krr.pred(Kt)
        
        return yt

# <codecell>

for param in xrange(9,10):
    files=["../dataset/model/4605.csv"]
    for fi in files:
        s=0
        df=pd.read_csv(fi)
        features=df.columns-['SalePrice','MachineID']
        if not np.any(df.columns=='SalePrice'):
            df=df.T
        df.YearMade=df.YearMade.astype('float')
        df.YearMade[df.YearMade==1000]=np.NaN
        kf=KFold(df.shape[0],k=10,indices=False)
        for train,test in kf:
            X=df[features][train]
            #print X.shape
            miss=transformer.Missingtransform()
            trans=transformer.Categoricaltransform()
            mult=transformer.MultiTrans()
            norm=preprocessing.Scaler()
            reg=tree.DecisionTreeRegressor(min_samples_leaf=100)#RandomForestRegressor(n_estimators=param)
            me=meanPred()
            if X.shape[0]<5:
                anova_svm = Pipeline([('mult',mult),('date',transformer.DateTrans()),('cat', trans),('miss', miss),('norm',norm),('reg',me)])#
            else:
                anova_svm = Pipeline([('mult',mult),('date',transformer.DateTrans()),('cat', trans),('miss', miss),('norm',norm),('reg',reg)])#
            
            anova_svm.fit(X,df['SalePrice'][train])
            #trans=anova_svm.transform(df[features][test])
            #print pd.isnull(trans).sum()
            
            #print np.isfinite(trans).sum()-trans.shape[0]*trans.shape[1]
            #print anova_svm.get_params()
            #print np.isnan(anova_svm.transform(X)[test]).sum()
            #break
            #print df[features][test]
            predict=np.maximum(anova_svm.predict(df[features][test]),np.zeros(df[features][test].shape[0]))
            #print predict
            #print df['SalePrice'][test]
            error=score_cus(np.asarray(df['SalePrice'][test]),predict)
            s+=error
            #print error
        print fi, s/10,param

# <codecell>

../dataset/model/77.csv 0.270598472036 9
../dataset/model/4605.csv 0.167103345695 9

