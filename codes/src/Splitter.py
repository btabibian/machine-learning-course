# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

%reset
import numpy as np
import pandas as pd

# <codecell>

data=pd.read_csv('../dataset/Train.csv')
data

# <codecell>

mdata=data.set_index(['ModelID'])
lst=np.asarray(mdata.index.tolist())

# <codecell>

for i in np.unique(lst):
    mdata.ix[i].to_csv('../dataset/model/'+str(i)+'.csv')

