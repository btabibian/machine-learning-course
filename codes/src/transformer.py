# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn import preprocessing
def category_to_int(data):
    uniques=np.unique(data)
    #print uniques.shape[0]
    return zip(uniques,xrange(1,uniques.shape[-1]+1))
class meanPred:
    def __init__(self):
        pass
    def fit(self,X,Y):
        self.mean=np.mean(Y,axis=1)
        return self
    def predict(self,X):
        return np.ones(X.shape[0])*self.mean
    def transform(self,X):
        return X
class Categoricaltransform(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.params=[]
        self.cols=[]
    def fit(self,X,y=None):
        for i in X:
            if (X[i].dtype=='object'):
                S=X[i][X[i].notnull()]
                array=np.asarray(S)
                ind_sorted=np.argsort(array)
                self.params.append(category_to_int(array[ind_sorted]))
            else:
                #print i,'is number'
                self.params.append([])
        self.cols=X.columns
        return self
    def transform(self,X):
        #print 'transforming'
        X_new=pd.DataFrame(np.zeros(X.shape)-1,index=X.index,columns=X.columns)
        for index,params in zip(self.cols,self.params):
            if(params==[]):
                X_new[index][:]=X[index][:]
            else:
                #print params
                for j in params:
                    X_new[index][X[index]==j[0]]=j[1]
            X_new[index][X_new[index]==-1]=np.NaN
            #print index
        return X_new
class Normalizetransform(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.params=[]
        self.cols=[]
    def fit(self,X,y=None):
        for i in X:
            if (X[i].dtype=='object'):
                self.params.append([])
            else:
                me=X[i].mean()
                sd=X[i].std()
                self.params.append([me,sd])
        self.cols=X.columns
        return self
    def transform(self,X):
        X_new=pd.DataFrame(X,copy=True)
        for index,params in zip(self.cols,self.params):
            if(params==[]):
                X_new[index][:]=X[index][:]
            else:
                X_new[index]=(X[index]-params[0])/params[1]
        #print 'cat',X_new
        return X_new
class Missingtransform(BaseEstimator, TransformerMixin):
    def __init__(self,fixed=None):
        self.params=[]
        self.cols=[]
        self.fixed=fixed
    def fit(self,X,y=None):
        for i in X:
            if (X[i].dtype=='object'):
                X_notnull=X[i][X[i].notnull()]
                self.params.append(X_notnull.value_counts().idxmax())
            else:
                X_notnull=X[i][X[i].notnull()]
                self.params.append(X_notnull.median())
        self.cols=X.columns
        return self
    def transform(self,X):
        X_new=pd.DataFrame(X)
        for index,params in zip(self.cols,self.params):
            if (self.fixed!=None):
                #print 'fixed'
                X_new[index][X_new[index].isnull()]=self.fixed
            else:
                X_new[index][X_new[index].isnull()]=params
        #print 'missing',X_new
        return X_new
class MultiTrans(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.params=[]
        self.cols=[]
    def fit(self,X,y=None):
        new_cols=[]
        for i in X.columns:
            value=X[i].value_counts()
            if value.shape[0]>1:
                self.params.append(i)
                #print i,value.shape[0]
        return self
    def transform(self,X):
        X_new=pd.DataFrame(X)
        return X_new[self.params]
class DateTrans(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.params=[]
        self.cols=[]
    def conv_date(self,d):
        try:
            return datetime.datetime.strptime(d,"%m/%d/%Y %H:%M")
        except:
            return np.NaN
    def conv_days(self,d):
        return d.days
    def fit(self,X,y=None):
        new_cols=[]
        for i in X.columns:
            sale_date=X[i].apply(self.conv_date)
            if np.sum(sale_date.notnull()>0):
                self.params.append((i,sale_date.min()))
                
        return self
    def transform(self,X):
        X_new=pd.DataFrame(X)
        for i,mi in self.params:
            sale_date=X_new[i].apply(self.conv_date)
            sale_date_dur=sale_date-mi
            X_new[i]=sale_date_dur.apply(self.conv_days)
            X_new[i+"_year"]=sale_date_dur.apply(lambda x: x.year)
            X_new[i+"_month"]=sale_date_dur.apply(lambda x: x.month)
        return X_new
        
class X2Trans(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.params=[]
        self.cols=[]
    def fit(self,X,y=None):
        return self
    def transform(self,X):
        X_new=pd.DataFrame(X)
        cols=X_new.columns
        for i in cols:
            for j in cols:
                X_new[str(i)+'-'+str(j)]=X_new[i]*X_new[j]
        return X_new
if __name__=='__main__':
    df=pd.read_csv("../dataset/model/82.csv")
    tran=Normalizetransform()
    tran.fit(df)
    tran.params
    print df['YearMade']
    print tran.transform(df)['YearMade']

